<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191121091525 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
        INSERT INTO `hotel` (`id`, `name`)
        VALUES
            (1, \'Scandic Opalen\'),
            (2, \'Boardinghouse St.Pauli\'),
            (3, \'Hotel Indigo Berlin – Ku’damm\'),
            (4, \'Biz Apartment Gärdet\');
        ');

        $this->addSql("
        INSERT INTO `review` (`id`, `hotel_id`, `text`, `created_at`)
        VALUES
            (1, 1, 'Very good breakfast. Like that they don\'t clean the room everyday, like they think about the environment', '2019-11-21 10:04:00'),
            (2, 1, 'The breakfast buffet spread was delightful!', '2019-11-20 19:55:00'),
            (3, 1, 'The breakfast was great and the straff was really helpful. Location was good and close to Liseberg. Well worth the money!', '2019-11-21 09:22:00'),
            (4, 2, 'Very, very friendly staff. Helpful and always in a good mood. Next time we will definitely stay there again.', '2019-11-21 09:09:00'),
            (5, 2, 'It was awesome. Location is perfect. You have every possible shop nearby ! Will be back 100%', '2019-11-21 07:33:00'),
            (6, 3, 'The hotel is excellent, it is really comfy, the location is great and very central. The staff was very friendly and helpful. We loved the possibility to make ourselves coffee, tea or hot chocolate in our room free of charge.', '2019-11-21 09:45:00'),
            (7, 3, 'Friendly happy faces upon arrival and departure. Nothing was too much trouble. it was a very popular place and kept very clean. Good location for train station, bar and restaurants etc...', '2019-11-21 06:54:00');
        ");
    }

    public function down(Schema $schema) : void
    {

    }
}
