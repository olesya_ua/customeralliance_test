<?php

namespace App\Service;

use App\Entity\Hotel;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class HotelService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var AdapterInterface */
    private $cache;

    public function __construct(EntityManagerInterface $entityManager, AdapterInterface $cache)
    {
        $this->entityManager = $entityManager;
        $this->cache = $cache;
    }

    /**
     * @param int $id
     *
     * @return Hotel|null
     *
     * @throws InvalidArgumentException
     */
    public function getHotelById(int $id): ?Hotel
    {
        $hotelFromCache = $this->cache->getItem(sprintf('hotel_%s', $id));
        $hotelFromCache->expiresAfter(60);

        if (!$hotelFromCache->isHit()) {
            $hotelRepository = $this->entityManager->getRepository(Hotel::class);
            $hotel = $hotelRepository->find($id);

            $this->cache->save($hotelFromCache->set($hotel));
        } else {
            $hotel = $hotelFromCache->get();
        }

        return $hotel;
    }
}
