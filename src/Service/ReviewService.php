<?php

namespace App\Service;

use App\Entity\Review;
use App\Repository\ReviewRepository;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\AdapterInterface;

class ReviewService
{
    /**
     * @var ReviewRepository
     */
    private $reviewRepository;

    /** @var AbstractAdapter */
    private $cache;

    public function __construct(ReviewRepository $reviewRepository, AdapterInterface $cache)
    {
        $this->reviewRepository = $reviewRepository;
        $this->cache = $cache;
    }

    /**
     * @param int $hotelId
     *
     * @return Review|null
     *
     * @throws InvalidArgumentException
     */
    public function getRandomReviewOfToday(int $hotelId): ?Review
    {
        $reviewFromCache = $this->cache->getItem(sprintf('hotel_rand_review_%s', $hotelId));
        $reviewFromCache->expiresAfter(60);

        if (!$reviewFromCache->isHit()) {
            $review = $this->reviewRepository->getRandomReviewOfToday($hotelId);
            $this->cache->save($reviewFromCache->set($review));
        } else {
            $review = $reviewFromCache->get();
        }

        return $review;
    }
}
