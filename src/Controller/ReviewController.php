<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use App\Service\HotelService;
use App\Service\ReviewService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ReviewController extends AbstractController
{
    /**
     * @var HotelService
     */
    private $hotelService;

    /**
     * @var ReviewService
     */
    private $reviewService;

    public function __construct(HotelService $hotelService, ReviewService $reviewService)
    {
        $this->hotelService = $hotelService;
        $this->reviewService = $reviewService;
    }

    /**
     * @Cache(expires="+5 minutes", smaxage="300")
     * @Route("/{hotelId}/today/review", name="rand_review", requirements={"hotelId"="\d+"})
     *
     * @param int $hotelId
     *
     * @return Response
     *
     * @throws InvalidArgumentException
     */
    public function randReviewAction(int $hotelId): Response
    {
        $hotel = $this->hotelService->getHotelById($hotelId);

        if (!$hotel) {
            throw $this->createNotFoundException('The hotel id was not found.');
        }

        return $this->render('review.html.twig', [
            'hotel' => $hotel,
            'review' => $this->reviewService->getRandomReviewOfToday($hotelId)
        ]);
    }
}
