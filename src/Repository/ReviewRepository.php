<?php

namespace App\Repository;

use App\Entity\Review;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;

/**
 * @method Review|null find($id, $lockMode = null, $lockVersion = null)
 * @method Review|null findOneBy(array $criteria, array $orderBy = null)
 * @method Review[]    findAll()
 * @method Review[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReviewRepository extends EntityRepository
{
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager, new Mapping\ClassMetadata(Review::class));
    }

    public function getRandomReviewOfToday(int $hotelId): ?Review
    {
        $queryBuilder = $this->createQueryBuilder('review');

        $reviewQueryResult = $queryBuilder->select('review')
            ->where($queryBuilder->expr()->eq('review.hotel', ':hotel'))
            ->andWhere($queryBuilder->expr()->gte('review.createdAt', ':today'))
            ->setParameters([
                'hotel' => $hotelId,
                'today' => date('Y-m-d 00:00:00')
            ])
            ->orderBy('RAND()')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return !empty($reviewQueryResult) ? array_shift($reviewQueryResult) : null;
    }
}
